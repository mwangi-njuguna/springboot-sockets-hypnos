package com.mwangi.hypnos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HypnosApplication {

	public static void main(String[] args) {
		SpringApplication.run(HypnosApplication.class, args);
	}

}
